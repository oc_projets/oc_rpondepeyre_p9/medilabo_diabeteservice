
# --------Build------------

FROM maven:3.9.5-amazoncorretto-21 AS builder
WORKDIR /app
COPY . .
RUN mvn package

# ---------RUN-----------

FROM amazoncorretto:21
WORKDIR /app
COPY  --from=builder /app/target/MedilaboDiabeteService-1.0.0.jar .

CMD [ "java", "-jar","MedilaboDiabeteService-1.0.0.jar"]