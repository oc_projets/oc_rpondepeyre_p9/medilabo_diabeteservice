package fr.medilabo.solutions.medilabodiabeteservice.config.enums;

import java.util.Arrays;
import java.util.List;

public class TriggerWords {

    public static final List<String> TriggerWords = Arrays.asList(
            "Hémoglobine A1C",
            "Microalbumine",
            "Taille",
            "Poids",
            "Fumeur",
            "Fumeuse",
            "Anormal",
            "Cholestérol",
            "Vertiges",
            "Rechute",
            "Réaction",
            "Anticorps");
}
