package fr.medilabo.solutions.medilabodiabeteservice.config.enums;

public enum DiabeteRiskEnum {
    NONE("None"),
    BORDERLINE("Limited Risk"),
    IN_DANGER("Danger"),
    EARLY_ONSET("Early Onset");

    private final String description;

    DiabeteRiskEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
