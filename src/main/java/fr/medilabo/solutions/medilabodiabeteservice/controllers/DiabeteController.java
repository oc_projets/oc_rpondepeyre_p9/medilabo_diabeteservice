package fr.medilabo.solutions.medilabodiabeteservice.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.medilabo.solutions.medilabodiabeteservice.services.DiabeteService;

@RestController
@RequestMapping("/diabete")
public class DiabeteController {

    private final DiabeteService service;

    public DiabeteController(DiabeteService service) {
        this.service = service;
    }

    @GetMapping("")
    public ResponseEntity<String> getDiabeteRisk(@RequestParam int id) {
        return new ResponseEntity<>(this.service.calculateDiabeteRisk(id).getDescription(), HttpStatus.OK);
    }

}
