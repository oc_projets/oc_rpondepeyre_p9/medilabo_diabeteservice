package fr.medilabo.solutions.medilabodiabeteservice.repositories;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import fr.medilabo.solutions.medilabodiabeteservice.models.Note;

@Repository
public class NoteRepository {

    @Value("${noteService.uri}")
    private String apiUrl;

    public List<Note> findNotesByPatientid(int id) {
        RestTemplate rest = new RestTemplate();
        Note[] arrays = rest.getForEntity(apiUrl + "/all?patientId=" + id, Note[].class).getBody();
        return Arrays.asList(arrays);

    }
}
