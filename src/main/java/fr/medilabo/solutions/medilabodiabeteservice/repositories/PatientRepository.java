package fr.medilabo.solutions.medilabodiabeteservice.repositories;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import fr.medilabo.solutions.medilabodiabeteservice.models.Patient;

@Repository
public class PatientRepository {

    @Value("${patientService.uri}")
    private String apiUrl;

    public Patient getPatientbyId(int id) {
        RestTemplate rest = new RestTemplate();
        return rest.getForEntity(apiUrl + "?id=" + id, Patient.class).getBody();
    }
}
