package fr.medilabo.solutions.medilabodiabeteservice.services;

import java.time.LocalDate;
import java.time.Period;
import java.util.HashSet;
import java.util.List;

import org.springframework.stereotype.Service;

import fr.medilabo.solutions.medilabodiabeteservice.config.enums.DiabeteRiskEnum;
import fr.medilabo.solutions.medilabodiabeteservice.config.enums.TriggerWords;
import fr.medilabo.solutions.medilabodiabeteservice.models.Note;
import fr.medilabo.solutions.medilabodiabeteservice.models.Patient;
import fr.medilabo.solutions.medilabodiabeteservice.repositories.NoteRepository;
import fr.medilabo.solutions.medilabodiabeteservice.repositories.PatientRepository;

@Service
public class DiabeteService {

    private final PatientRepository patientRepository;
    private final NoteRepository noteRepository;

    public DiabeteService(PatientRepository patientRepository, NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
        this.patientRepository = patientRepository;
    }

    public DiabeteRiskEnum calculateDiabeteRisk(int patientId) {
        Patient patient = patientRepository.getPatientbyId(patientId);
        List<Note> notes = noteRepository.findNotesByPatientid(patientId);

        int triggers = calculateListNotesTriggers(notes);
        int age = calculateAge(patient.getDatedenaissance());
        String gender = patient.getGenre();

        if (age < 30) {
            if (gender.equalsIgnoreCase("M")) {
                return calcDiabeteMaleUnder30(triggers);
            } else {
                return calcDiabeteFemaleUnder30(triggers);
            }
        } else {
            return calcDiabeteOver30(triggers);
        }
    }

    public DiabeteRiskEnum calcDiabeteOver30(int triggers) {
        if (triggers >= 8) {
            return DiabeteRiskEnum.EARLY_ONSET;
        } else if (triggers >= 6) {
            return DiabeteRiskEnum.IN_DANGER;
        } else if (triggers >= 2) {
            return DiabeteRiskEnum.BORDERLINE;
        } else {
            return DiabeteRiskEnum.NONE;
        }
    }

    public DiabeteRiskEnum calcDiabeteMaleUnder30(int triggers) {
        if (triggers >= 5) {
            return DiabeteRiskEnum.EARLY_ONSET;
        } else if (triggers >= 3) {
            return DiabeteRiskEnum.IN_DANGER;
        } else {
            return DiabeteRiskEnum.NONE;
        }
    }

    public DiabeteRiskEnum calcDiabeteFemaleUnder30(int triggers) {
        if (triggers >= 7) {
            return DiabeteRiskEnum.EARLY_ONSET;
        } else if (triggers >= 4) {
            return DiabeteRiskEnum.IN_DANGER;
        } else {
            return DiabeteRiskEnum.NONE;
        }
    }

    public int calculateListNotesTriggers(List<Note> notes) {
        HashSet<String> set = new HashSet<>();
        for (Note note : notes) {
            String message = note.getMessage().toLowerCase();
            for (String word : TriggerWords.TriggerWords) {
                if (message.contains(word.toLowerCase())) {
                    set.add(word);
                }
            }
        }
        return set.size();
    }

    public int calculateAge(LocalDate birthDate) {
        return Period.between(birthDate, LocalDate.now()).getYears();
    }
}
