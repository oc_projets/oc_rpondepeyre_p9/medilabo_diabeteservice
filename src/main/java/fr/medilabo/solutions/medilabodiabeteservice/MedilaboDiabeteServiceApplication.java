package fr.medilabo.solutions.medilabodiabeteservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedilaboDiabeteServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedilaboDiabeteServiceApplication.class, args);
	}

}
