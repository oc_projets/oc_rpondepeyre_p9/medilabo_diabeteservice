package fr.medilabo.solutions.MedilaboDiabeteService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.medilabo.solutions.medilabodiabeteservice.config.enums.DiabeteRiskEnum;
import fr.medilabo.solutions.medilabodiabeteservice.models.Note;
import fr.medilabo.solutions.medilabodiabeteservice.models.Patient;
import fr.medilabo.solutions.medilabodiabeteservice.repositories.NoteRepository;
import fr.medilabo.solutions.medilabodiabeteservice.repositories.PatientRepository;
import fr.medilabo.solutions.medilabodiabeteservice.services.DiabeteService;

@ExtendWith(MockitoExtension.class)
public class DiabeteServiceTest {

    @Mock
    PatientRepository patientRepository;

    @Mock
    NoteRepository noteRepository;

    @InjectMocks
    DiabeteService service;

    @Test
    public void calculateAgeTest() {
        LocalDate birthDate = LocalDate.now().minusYears(20);
        int result = service.calculateAge(birthDate);
        assertEquals(20, result);
    }

    @Test
    public void calculateListNoteTriggers() {
        Note note = new Note();
        note.setMessage("Anticorps Réaction Hémoglobine A1C untriggered Vertiges Cholestérol Anormal Réaction");

        List<Note> notes = new ArrayList<>();
        notes.add(note);
        notes.add(note);

        int result = service.calculateListNotesTriggers(notes);

        assertEquals(6, result);
    }

    @Test
    public void calculateDiabeteRisk_case1() {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(40));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Hémoglobine A1C Microalbumine Taille Poids Fumeuse Anormal Cholestérol Vertiges");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        DiabeteRiskEnum result = service.calculateDiabeteRisk(1);

        assertEquals(result, DiabeteRiskEnum.EARLY_ONSET);
    }

    @Test
    public void calculateDiabeteRisk_case2() {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(40));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Taille Poids Fumeuse Anormal Cholestérol Vertiges ");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        DiabeteRiskEnum result = service.calculateDiabeteRisk(1);

        assertEquals(result, DiabeteRiskEnum.IN_DANGER);
    }

    @Test
    public void calculateDiabeteRisk_case3() {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(40));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Cholestérol Vertiges");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        DiabeteRiskEnum result = service.calculateDiabeteRisk(1);

        assertEquals(result, DiabeteRiskEnum.BORDERLINE);
    }

    @Test
    public void calculateDiabeteRisk_case4() {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(40));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Not triggered");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        DiabeteRiskEnum result = service.calculateDiabeteRisk(1);

        assertEquals(result, DiabeteRiskEnum.NONE);
    }

    @Test
    public void calculateDiabeteRisk_case5() {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Microalbumine Taille Poids Fumeuse Anormal Cholestérol Vertiges");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        DiabeteRiskEnum result = service.calculateDiabeteRisk(1);

        assertEquals(result, DiabeteRiskEnum.EARLY_ONSET);
    }

    @Test
    public void calculateDiabeteRisk_case6() {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Poids Fumeuse Anormal Cholestérol");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        DiabeteRiskEnum result = service.calculateDiabeteRisk(1);

        assertEquals(result, DiabeteRiskEnum.IN_DANGER);
    }

    @Test
    public void calculateDiabeteRisk_case7() {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Microalbumine Taille Poids ");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        DiabeteRiskEnum result = service.calculateDiabeteRisk(1);

        assertEquals(result, DiabeteRiskEnum.NONE);
    }

    @Test
    public void calculateDiabeteRisk_case8() {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("M");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Microalbumine Taille Poids Fumeuse Anormal");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        DiabeteRiskEnum result = service.calculateDiabeteRisk(1);

        assertEquals(result, DiabeteRiskEnum.EARLY_ONSET);
    }

    @Test
    public void calculateDiabeteRisk_case9() {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("M");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Fumeur Taille Poids ");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        DiabeteRiskEnum result = service.calculateDiabeteRisk(1);

        assertEquals(result, DiabeteRiskEnum.IN_DANGER);
    }

    @Test
    public void calculateDiabeteRisk_case10() {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("M");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Fumeur Poids ");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        DiabeteRiskEnum result = service.calculateDiabeteRisk(1);

        assertEquals(result, DiabeteRiskEnum.NONE);
    }
}
