package fr.medilabo.solutions.MedilaboDiabeteService.TI;

import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import fr.medilabo.solutions.medilabodiabeteservice.MedilaboDiabeteServiceApplication;
import fr.medilabo.solutions.medilabodiabeteservice.models.Note;
import fr.medilabo.solutions.medilabodiabeteservice.models.Patient;
import fr.medilabo.solutions.medilabodiabeteservice.repositories.NoteRepository;
import fr.medilabo.solutions.medilabodiabeteservice.repositories.PatientRepository;

@SpringBootTest(classes = MedilaboDiabeteServiceApplication.class)
@AutoConfigureMockMvc
public class DiabeteControllerTI {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    NoteRepository noteRepository;

    @MockBean
    PatientRepository patientRepository;

    @Test
    public void Diabete_Risk_Case1() throws Exception {

        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(40));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Hémoglobine A1C Microalbumine Taille Poids Fumeuse Anormal Cholestérol Vertiges");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        mockMvc.perform(MockMvcRequestBuilders.get("/diabete")
                .param("id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Early Onset"));

    }

    @Test
    public void Diabete_Risk_Case2() throws Exception {

        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(40));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Taille Poids Fumeuse Anormal Cholestérol Vertiges ");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        mockMvc.perform(MockMvcRequestBuilders.get("/diabete")
                .param("id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Danger"));

    }

    @Test
    public void Diabete_Risk_Case3() throws Exception {

        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(40));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Cholestérol Vertiges");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        mockMvc.perform(MockMvcRequestBuilders.get("/diabete")
                .param("id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Limited Risk"));

    }

    @Test
    public void Diabete_Risk_Case4() throws Exception {

        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(40));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Not triggered");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        mockMvc.perform(MockMvcRequestBuilders.get("/diabete")
                .param("id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("None"));

    }

    @Test
    public void Diabete_Risk_Case5() throws Exception {

        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Microalbumine Taille Poids Fumeuse Anormal Cholestérol Vertiges");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        mockMvc.perform(MockMvcRequestBuilders.get("/diabete")
                .param("id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Early Onset"));

    }

    @Test
    public void Diabete_Risk_Case6() throws Exception {

        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Poids Fumeuse Anormal Cholestérol");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        mockMvc.perform(MockMvcRequestBuilders.get("/diabete")
                .param("id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Danger"));

    }

    @Test
    public void Diabete_Risk_Case7() throws Exception {

        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("F");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Microalbumine Taille Poids ");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        mockMvc.perform(MockMvcRequestBuilders.get("/diabete")
                .param("id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("None"));

    }

    @Test
    public void Diabete_Risk_Case8() throws Exception {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("M");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Microalbumine Taille Poids Fumeuse Anormal");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        mockMvc.perform(MockMvcRequestBuilders.get("/diabete")
                .param("id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Early Onset"));

    }

    @Test
    public void Diabete_Risk_Case9() throws Exception {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("M");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Fumeur Taille Poids ");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        mockMvc.perform(MockMvcRequestBuilders.get("/diabete")
                .param("id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("Danger"));

    }

    @Test
    public void Diabete_Risk_Case10() throws Exception {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setDatedenaissance(LocalDate.now().minusYears(20));
        patient.setGenre("M");

        Note note = new Note();
        note.setPatientId(1);
        note.setMessage("Fumeur Poids ");
        List<Note> notes = new ArrayList<>();
        notes.add(note);

        when(patientRepository.getPatientbyId(1)).thenReturn(patient);
        when(noteRepository.findNotesByPatientid(1)).thenReturn(notes);

        mockMvc.perform(MockMvcRequestBuilders.get("/diabete")
                .param("id", "1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("None"));

    }

}
